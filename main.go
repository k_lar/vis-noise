package main

import (
	"fmt"
	"math/rand"
	"os"
	"bufio"
	"time"
)

const height = 10
const width = 10

func fillGrid(grid [][]string, symbols []string) {
	for i := range grid {
		for j := range grid[i] {
			grid[i][j] = symbols[rand.Intn(len(symbols))]
		}
	}
}

func main() {
	grid := make([][]string, height)
	for i := range grid {
		grid[i] = make([]string, width)
	}

	symbols := []string{" ", ".", "-", "=", "c", "o", "a", "A", "@", "#"}

	for {
		clear := ""
		fillGrid(grid, symbols)

		w := bufio.NewWriter(os.Stdout)
		for _, row := range grid {
			for _, cell := range row {
				data := fmt.Sprintf("%s ", cell)
				fmt.Fprint(w, data)
			}
			fmt.Fprint(w, "\n")
		}
		w.Flush()

		w2 := bufio.NewWriter(os.Stdout)
		for i := 0; i < height; i++ {
			clear = clear + fmt.Sprintf("\033[1A\033[K")
			fmt.Fprint(w2, clear)
		}

		time.Sleep(1 * time.Millisecond)
		w2.Flush()
	}
}
